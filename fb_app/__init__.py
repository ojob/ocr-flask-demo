# -*- coding: utf-8 -*-
"""Entry point."""

# ---- third-party imports ------------------------------------------------

# ---- local imports ------------------------------------------------------
from . import models
from .views import app

# ---- init code ----------------------------------------------------------
models.db.init_app(app)


# ---- scripts ------------------------------------------------------------
@app.cli.command()
def init_db():
    """Initialize the db."""
    models.init_db()
