# -*- coding: utf-8 -*-
"""Pages served by Flask."""

from flask import Flask

app = Flask(__name__)

app.config.from_object('config')
# et pour accéder à une valeur: app.config[<varname>]


@app.route('/')
def index():
    return "Hello World!"


# if __name__ == '__main__':
#     app.run()
