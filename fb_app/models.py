# -*- coding: utf-8 -*-
"""Models of the app."""
# ---- built-in imports ---------------------------------------------------
import logging

# ---- third-party imports ------------------------------------------------
from flask_sqlalchemy import SQLAlchemy

# ---- local imports ------------------------------------------------------
from .views import app

# --
db = SQLAlchemy(app)


# ---- tables declaration -------------------------------------------------
class Content(db.Model):
    """Table for content description."""

    id = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.String(200), nullable=False)
    gender = db.Column(db.Integer(), nullable=False)

    def __init__(self, description, gender):
        self.description = description
        self.gender = gender


# ---- functions definition ----------------------------------------------
def init_db():
    """Populate the database with stuff."""
    db.drop_all()
    db.create_all()
    db.session.add(Content("THIS IS SPAARTAAAA!!", 1))
    db.session.add(Content("What's your favorite scary movie", 0))
    db.session.commit()

    logging.info("Database initialized")


# ---- module code --------------------------------------------------------
db.create_all()
