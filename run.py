# -*- coding: utf-8 -*-
"""Start the server, with our application."""

from fb_app import app

if __name__ == '__main__':
    app.run(debug=True)
