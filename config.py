# -*- coding: utf-8 -*-
"""Configuration of the app; out of the code base."""

import os

# ---- constants ----------------------------------------------------------
ROOT_DIR = os.path.abspath(os.path.dirname(__file__))

FB_APP_ID = '471988476990314'

# To generate a new secret key:
# >>> import random, string
# >>> "".join([random.choice(string.printable) for _ in range(24)])
SECRET_KEY = '}X{Z;gf~VR`r4gO,d\n19^Ua,'

SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(ROOT_DIR, 'app.db')
SQLALCHEMY_TRACK_MODIFICATIONS = True
